<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $html_title; ?></title>
    <link href="css/install.css" rel="stylesheet" type="text/css">
    <link href="css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
</head>
<body>
<?php echo $html_header; ?>
<div class="main">
    <div class="text-box" id="text-box">
        <div class="license">
            <h1>系统安装协议</h1>
            <p style="font-size: 15px">欢迎使用Timely在线客服系统，Timely在线客服系统采用了ThinkPHP5.1、Layui、Swoole，是一款轻量级、高速度的PHP在线客服系统。
                您在使用中如有任何问题都可以登录Timely官方网站获取帮助。官方网址为 http://www.cnavd.com。</p>

            <h3>I. 协议许可的权利</h3>
            <ol>
                <li>Timely遵循Apache Lisense 2.0开源协议发布，并提供免费使用。</li>
                <li> Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，鼓励代码共享和尊重原作者的著作权，允许代码修改，再作为开源或商业软件发布。需要满足的条件：</li>
                <li> 1、需要给用户一份Apache Licence ；</li>
                <li> 2、如果你修改了代码，需要在被修改的文件中说明；</li>
                <li> 3、在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明；</li>
                <li> 4、如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有本协议内容。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。</li>
                <li>您可以在本软件基础上进行修改、再编辑、甚至应用于商业项目，但您必须保留本软件相关标识。</li>
            </ol>
            <h3>II. 免责声明</h3>
            <ol>
                <li>1、使用Timely构建的网站的任何信息内容以及导致的任何版权纠纷和法律争议及后果，Timely官方不承担任何责任。</li>
                <li>2、 您一旦安装使用Timely，即被视为完全理解并接受本协议的各项条款，在享有上述条款授予的权力的同时，受到相关的约束和限制。</li>

            </ol>
            <p></p>

            <p></p>
            <p align="right">Timely研发团队</p>
            <p align="right">2019年12月1日</p>
        </div>
    </div>
    <div class="btn-box"><a href="index.php?step=1" class="btn btn-primary">同意协议进入安装</a><a
                href="javascript:window.close()" class="btn">不同意</a></div>
</div>
<?php echo $html_footer; ?>
<script type="text/javascript">
    $(document).ready(function () {
        //自定义滚定条
        $('#text-box').perfectScrollbar();
    });
</script>
</body>
</html>
