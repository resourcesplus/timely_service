<?php
/**
 * Created by PhpStorm.
 * User: zhc
 * Date: 2019/10/29
 * Time: 16:38
 */

namespace app\index\controller;


use think\Controller;
use think\Db;
use think\facade\Cache;

class Login extends Base
{
    protected $_require_login = false;

    public function Login()
    {
        return $this->fetch();
    }

    public function LoginIn()
    {

        $name = input('post.username');
        $password = input('post.password');
        $captcha = input('post.captcha');
        if (!captcha_check($captcha)) {
            return $this->response->api('', self::ERROR_CODE, '验证码错误');
        }
        if (empty($name)) {
            return $this->response->api('', self::ERROR_CODE, '请填写客服名');
        }
        if (empty($password)) {
            return $this->response->api('', self::ERROR_CODE, '请填写密码');
        }
        $kefu_info = Db::name('kefu_info')->where('kefu_name', $name)->find();
        if (!$kefu_info) {
            return $this->response->api('', self::ERROR_CODE, '客服不存在');
        }
        if (strtoupper(md5($password)) != $kefu_info['kefu_password']) {
            return $this->response->api('', self::ERROR_CODE, '密码错误');
        }
           session('kefu_name', $kefu_info['kefu_name']);
           session('kefu_code', $kefu_info['kefu_code']);
           session( 'kefu_avatar', $kefu_info['kefu_avatar']);


       return $this->response->api(url('kefu/index'), self::SUCCESS_CODE, '登录成功');
    }

    public function logout()
    {
        session('kefu_name', null);
        session('kefu_code', null);
        session('kefu_avatar', null);
        return $this->redirect('login/login');
    }
}
