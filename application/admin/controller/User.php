<?php
/**
 * Created by PhpStorm.
Visitor
 * Date: 2019/12/16
 * Time: 17:32
 */

namespace app\admin\controller;

use Logic\AdminLogic;

class User extends Base

{

    /**
     * 用户列表
     * @param $param
     * @return array
     */
    public function index()
    {
        if (request()->isAjax()) {
            $limit = input('param.limit');
            $where = [];
            if (input('param.account')) {
                $where[] = ['account', 'like', '%' . input('param.account') . '%'];
            }
            $list = AdminLogic::getAdminList($where, '*', $limit);

            return $this->response->api($list->items() ? $list->items() : '', self::SUCCESS_CODE, '操作成功', ['count' => $list->total()]);
        }
        return $this->fetch();
    }

}
