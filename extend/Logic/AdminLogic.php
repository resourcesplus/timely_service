<?php
/**
 * Created by PhpStorm.
 * Author: baihu
 * Date: 2019/12/16
 * Time: 15:29
 */

namespace Logic;

use think\Db;
use exception\LogicException;
use exception\BaseException;
use think\Exception;
use exception\ResponsableException;
class AdminLogic
{
    /**
     * 登录逻辑
     * @param $account 账号
     * @param $password 密码
     * @return array
     */
    public static function LoginAdmin($account, $password)
    {
        try {

            $info = Db::name('admin')->where('account', $account)->find();
            if (!$info) {
                throw new LogicException('账户不存在', 1001);
            }
            if (strtoupper(md5($password)) != $info['password']) {
                throw new LogicException('密码错误', 1002);
            }
            return $info;
        } catch (BaseException $be) {
            throw new ResponsableException($be->getMessage(), $be->getCode());
        } catch (Exception $e) {
            throw new ResponsableException('登录失败', 404);
        }
    }
    /**
     * 获取管理员列表 分页
     * @param $where 查询条件
     * @param $filed 显示字段
     * @param $page 页码
     * @param$limit  条数
     * @return array
     */
    public static function getAdminList($where, $filed = "*", $limit = 20)
    {
        try {
            return Db::name('admin')->field($filed)->where($where)->paginate( $limit);
        } catch (Exception $e) {
            throw new ResponsableException('信息错误', 404);
        }

    }
}
